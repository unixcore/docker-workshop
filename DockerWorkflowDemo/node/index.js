var express = require('express'),
    http = require('http'),
    redis = require('redis'),
    os = require('os');

var app = express();

console.log(process.env.REDIS_PORT_6379_TCP_ADDR + ':' + process.env.REDIS_PORT_6379_TCP_PORT);

// APPROACH 1: Using environment variables created by Docker
// var client = redis.createClient(
// 	process.env.REDIS_PORT_6379_TCP_PORT,
//   	process.env.REDIS_PORT_6379_TCP_ADDR
// );

// APPROACH 2: Using host entries created by Docker in /etc/hosts (RECOMMENDED)
var client = redis.createClient('6379', 'redis');


app.get('/', function(req, res, next) {
  client.incr('counter', function(err, counter) {
    if (err)
      return next(err);
    var message = 'I am Version 1.0<br>' +
        'This page has been viewed ' + counter + ' times!<br>' +
        '<small>Served By: ' + os.hostname() + ' </small><br>';
    console.log('Serving /')
    res.send(message);
  });
});

app.get('/kill-me', function(req, res, next) {
  res.send('killing ' + os.hostname())
  process.exit();
})

http.createServer(app).listen(process.env.PORT || 8080, function() {
  console.log('Listening on port ' + (process.env.PORT || 8080));
});
